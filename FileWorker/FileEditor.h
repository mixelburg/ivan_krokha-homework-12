#pragma once
#include <mutex>
#include <fstream>

class SynchronizedFile {
public:
    SynchronizedFile(const std::string& path) : _path(path), _isReading(0), _isWriting(false)
	{
        _file.open(path);
    }
	~SynchronizedFile()
    {
	    try
	    {
            _file.close();
	    }
	    catch (...)
	    {
	    }
    }

    void write(const std::string& dataToWrite)
	{
        // Ensure that only one thread can execute at a time

        std::unique_lock<std::mutex> lock(_writerMutex);
        _writeCV.wait(lock, [this] { return !_isWriting && _isReading == 0; });

        _isWriting = true;

    	// TODO: write data to file

    	_isWriting = false;

        lock.unlock();
        _readCV.notify_all();
        _writeCV.notify_all();
    }

	void read(const int rowNum)
    {
        std::unique_lock<std::mutex> lock;
        _readCV.wait(lock, [this] { return !_isWriting; });
        _isReading++;

        // TODO: read data from file

        _isReading--;
    	
        _readCV.notify_all();
        _writeCV.notify_all();
    }

private:
    std::string _path;
    std::ifstream _file;
    std::mutex _writerMutex;
	
    std::condition_variable _readCV;
    std::condition_variable _writeCV;
    int _isReading;
    bool _isWriting;
};

class Writer {
public:
    Writer(std::shared_ptr<SynchronizedFile> sf) : _sf(sf) {}

    void write() {
        // Do some work...
        _sf->write("Some data to write...");
    }
private:
    std::shared_ptr<SynchronizedFile> _sf;
};

class Reader
{
public:
    Reader(std::shared_ptr<SynchronizedFile> sf) : _sf(sf) {}

    void read() {

    	_sf->read(6);
    }
private:
    std::shared_ptr<SynchronizedFile> _sf;
};
