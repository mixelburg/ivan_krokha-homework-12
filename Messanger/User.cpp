#include "User.h"

User::User(std::string& name) : _name(name), _connected(false)
{
}

User::User(std::string& name, const bool status) : _name(name), _connected(status)
{
}

void User::changeStatus()
{
	_connected = !_connected;
}

std::string User::getName() const
{
	return _name;
}

bool operator==(const User& ths, const User& other)
{
	return ths._name == other._name;
}

bool operator<(const User& ths, const User& other)
{
	return  ths._name < other._name;
}
