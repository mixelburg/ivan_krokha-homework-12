#pragma once

#include <mutex>
#include <set>
#include <queue>

#include "User.h"
#include "MyUtil.h"


class OpenFileException final : public std::exception
{
public:
	OpenFileException(const std::string fName, std::string msg = "[!] Error opening file") : std::exception(msg.c_str()), _fileName(fName) {}
	std::string getFile() const { return _fileName; }
private:
	const std::string _fileName;
};

class UserIsNotConnectedException final : public std::exception
{
	const char* what() const throw() override
	{
		return "[!] User is not connected";
	}
};

class UserIsAlreadyConnectedException final : public std::exception
{
	const char* what() const throw() override
	{
		return "[!] User already connected";
	}
};

class MessageSender
{
public:
	/**
	 * @brief adds given User object to connected users list
	 * @param user User object to add
	 * @throws UserIsAlreadyConnectedException if user is already connected
	*/
	void connectUser(User& user);
	void connectUser(std::string& userName); // connects user by name
	void connectUser(); // gets user name to add from user

	/**
	 * @brief removes given User object from list of connected users
	 * @param user User object to remove
	 * @throws UserIsNotConnectedException if given user is not connected
	*/
	void removeUser(User& user);
	void removeUser(std::string& username); // removes user by name
	void removeUser(); // gets user name to remove from user

	void printConnected() const; // prints names of all connected users

	void clearFile() const; // flushes file content

	/**
	 * \brief reads FILE content line by line and appends it to _msgQueue than cleans the file
	 * repeats the process every INTERVAL seconds
	 */
	[[noreturn]] void readFile();

	[[noreturn]] void sendMessages();

	/**
	 * @brief runs readFile and sendMessages in threads
	*/
	void run();
private:
	std::set<User> _connectedUsers; // list of all connected users
	std::queue<std::string> _msgQueue; 
	std::mutex _msgQueueMutex;
	std::condition_variable _messageReadyCV;
	
	bool isMessage() const;
	bool isConnected(User& user) const; // checks if user is in list of connected

	const std::string _enterNameMsg = "Enter your name: ";
	const std::string IN_FILE = "data.txt";
	const std::string OUT_FILE = "output.txt";
	const int INTERVAL = 5; // repeat interval for readFile function
};



