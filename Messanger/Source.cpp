#include "MessageSender.h"

int main()
{
	MessageSender msgSender;
	std::string name = "Alina";
	std::string name1 = "Anita";
	msgSender.connectUser(name);
	msgSender.connectUser(name1);
	
	msgSender.run();

	return 0;
}