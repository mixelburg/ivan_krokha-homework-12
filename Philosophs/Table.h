#pragma once
#include <iostream>
#include <mutex>

class Philosoph
{
public:
	explicit Philosoph(const int index) : _index(index) {}
	Philosoph() : _index(-1) {}
	void eat()
	{
		std::cout << "[+] Eating: " << _index << std::endl;
		_hunger--;
		std::cout << "[+] Finished eating" << std::endl;
	}

	int getIndex() const;
	void setIndex(const int i);
	bool isHungry() const;
private:
	int _index;
	int _hunger = 100;
};

class Table
{
public:
	static int getSecondIndex(int lIndex);

	void eat(Philosoph& philosoph);

	void run();

private:
	bool _sticks[5] = { true, true, true, true, true };
	
	std::mutex _eatMutex;
	std::condition_variable _eating;

	bool checkEat(const int index);
	bool checkEat(const Philosoph& philosoph);
};

